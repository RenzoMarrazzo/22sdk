Pod::Spec.new do |s|

  s.name         = "MyRadioSDK"
  s.version      = "0.0.1"
  s.summary      = "SDK to use the 22HBG FORCE Services"
  s.homepage     = "https://bitbucket.org/RenzoMarrazzo/22sdk"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author       = { "Renzo" => "renzo@22hbg.com" }
  s.ios.deployment_target = "10.2"
  s.source       = { :git => "https://bitbucket.org/RenzoMarrazzo/22sdk", :tag => "#{s.version}" }
  s.source_files  = '22HBGSDK', '22HBGSDK/**/*.{h,swift}'
  s.dependency "Alamofire"

end
