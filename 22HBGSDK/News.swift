//
//  News.swift
//  fmworld
//
//  Created by Oronzo Marrazzo on 16/01/17.
//  Copyright © 2017 Oronzo Marrazzo. All rights reserved.
//

import Foundation

public class News {
    
    public var id : String?
    var source : String?
    var title : String?
    var date : String?
    var imageUrl : String?
    
    //Detail
    var content : String?
    
    func parseNewsFromDict(newsDict : NSDictionary) -> News {
        
        if let id : String = newsDict["id"] as? String {
            self.id = id
        }
        if let source : String = newsDict["source"] as? String {
            self.source = source
        }
        if let title : String = newsDict["title"] as? String {
            self.title = title
        }
        if let date : String = newsDict["date"] as? String {
            self.date = date
        }
        if let imageUrl : String = newsDict["image"] as? String {
            self.imageUrl = imageUrl
        }
        if let content : String = newsDict["content"] as? String {
            self.content = content
        }
        
        return self
    }
}
