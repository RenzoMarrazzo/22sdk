//
//  Stream.swift
//  fmworld
//
//  Created by Oronzo Marrazzo on 16/01/17.
//  Copyright © 2017 Oronzo Marrazzo. All rights reserved.
//

import Foundation

public class Stream {
    
    var id : String?
    var codec : String?
    var server : String?
    var url : String?
    var channels : Int?
    var bitrate : Int?
    var quality : Int?
    var isOnline : Bool?
    
    init() {}
    
    init(url : String) {
        
        self.url = url
    }
    
    func parseStreamFromDictionary(streamDict : NSDictionary) -> Stream {
        
        if let id : Int = streamDict["id"] as? Int {
            self.id = String(id)
        }
        if let codec : String = streamDict["codec"] as? String {
            self.codec = codec
        }
        if let server : String = streamDict["server"] as? String {
            self.server = server
        }
        if let url : String = streamDict["url"] as? String {
            self.url = url
        }
        if let channels : Int = streamDict["channels"] as? Int {
            self.channels = channels
        }
        if let bitrate : Int = streamDict["bitrate"] as? Int {
            self.bitrate = bitrate
        }
        if let quality : Int = streamDict["quality"] as? Int {
            self.quality = quality
        }
        if let isOnline : Bool = streamDict["is_online"] as? Bool {
            self.isOnline = isOnline
        }
        
        return self
        
    }
}
