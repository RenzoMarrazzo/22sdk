//
//  Frequence.swift
//  fmworld
//
//  Created by Oronzo Marrazzo on 16/01/17.
//  Copyright © 2017 Oronzo Marrazzo. All rights reserved.
//

import Foundation
public class Frequency {
    
    var radioID : String?
    var radioName : String?
    var frequency : String?
    var stereo : Bool?
    var cityName : String?
    var provinceName : String?
    var provinceCode : String?
    
    func parseFrequencyFromDict(frequencyDict : NSDictionary) -> Frequency {
        
        if let radioID : Int = frequencyDict["radio_id"] as? Int {
            self.radioID = String(radioID)
        }
        if let radioName : String = frequencyDict["radio_name"] as? String {
            self.radioName = radioName
        }
        if let frequency : String = frequencyDict["frequency"] as? String {
            self.frequency = frequency
        }
        if let stereo : Bool = frequencyDict["stereo"] as? Bool {
            self.stereo = stereo
        }
        if let cityName : String = frequencyDict["city_name"] as? String {
            self.cityName = cityName
        }
        if let provinceName : String = frequencyDict["province_name"] as? String {
            self.provinceName = provinceName
        }
        if let provinceCode : String = frequencyDict["province_code"] as? String {
            self.provinceCode = provinceCode
        }
        
        return self
        //
        
    }
}
