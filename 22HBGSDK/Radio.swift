//
//  Radio.swift
//  fmworld
//
//  Created by Oronzo Marrazzo on 16/01/17.
//  Copyright © 2017 Oronzo Marrazzo. All rights reserved.
//

import Foundation

public class Radio {
 
    var id : String?
    var position : Int?
    var imageID : Int?
    
    var name : String?
    var facebook : String?
    var twitter : String?
    var webSite : String?
    var imageUrl : String?
    
    var streams : [Stream]?
    
    init() {}
    
    public init(name: String,imageUrl : String,streamUrl : String){
        
        self.name = name
        self.imageUrl = imageUrl
        self.streams = []
        self.streams?.append(Stream(url: streamUrl))
        
    }
    
    func parseRadioFromDict(radioDict : NSDictionary) -> Radio {
        
        if let id : Int = radioDict["id"] as? Int {
            self.id = String(id)
        }
        if let position : Int = radioDict["position"] as? Int {
            self.position = position
        }
        if let name : String = radioDict["name"] as? String {
            self.name = name
        }
        if let facebook : String = radioDict["facebook"] as? String {
            self.facebook = facebook
        }
        if let twitter : String = radioDict["twitter"] as? String {
            self.twitter = twitter
        }
        if let webSite : String = radioDict["website"] as? String {
            self.webSite = webSite
        }
        if let imageID : Int = radioDict["image_id"] as? Int {
            self.imageID = imageID
        }
        if let streams : NSArray = radioDict["streams"] as? NSArray {
            
            self.streams = []
            
            for streamItem in streams {
                
                let stream : Stream = Stream().parseStreamFromDictionary(streamDict: (streamItem as! NSDictionary))
                self.streams!.append(stream)
                
            }
        }
        
        return self
        
    }
    
}
