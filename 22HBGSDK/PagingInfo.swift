//
//  PagingInfo.swift
//  fmworld
//
//  Created by Oronzo Marrazzo on 16/01/17.
//  Copyright © 2017 Oronzo Marrazzo. All rights reserved.
//

import Foundation
public class PagingInfo {
    
    var setSize : Int?
    var setPage : Int?
    var totalResults : Int?
    var totalPages : Int?
    
    func parsePagingFromDict(pagingDict : NSDictionary) -> PagingInfo {
        if let setSize : String = pagingDict["set_size"] as? String {
            self.setSize = Int(setSize)
        }
        if let setPage : String = pagingDict["set_page"] as? String {
            self.setPage = Int(setPage)
        }
        if let totalResults : Int = pagingDict["total_result"] as? Int {
            self.totalResults = totalResults
        }
        if let totalPages : Int = pagingDict["total_page"] as? Int {
            self.totalPages = totalPages
        }
        
        return self
    }
}
