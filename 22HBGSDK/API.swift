//
//  DataManager.swift
//  fmworld
//
//  Created by Oronzo Marrazzo on 16/01/17.
//  Copyright © 2017 Oronzo Marrazzo. All rights reserved.
//

import Foundation
import Alamofire

public class API {
    
    let kURL_RADIO_LIST = "http://api2.fm-world.com/v1.2/apps/mobile/d0b5fcf9c329e5b6af7a2fb094e0da1e4c2a4739"
    let kURL_NEWS = "http://api2.fm-world.com/v1.3/news?"
    let kURL_NEWS_DETAIL = "http://api2.fm-world.com/v1.3/news/"
    let kURL_FREQUENCIES = "http://api2.fm-world.com/v1.3/radios/geolocate/all?"
    let kURL_RADIO_FREQUENCIES = "http://api2.fm-world.com/v1.2/radios/"

    public static let sharedInstance = API()
    
    public func getRadios(callback: @escaping (NSError?,[Radio]?) -> ()){
        
        print("\n *** getRadios()")
        
        Alamofire.request(
            kURL_RADIO_LIST)
            .responseJSON { response in
                
                switch response.result {
                    
                case .success(let JSON) :
                    
                    print("\n ****** getRadios() -> Success")
                    
                    let responseDictionary = JSON as! NSDictionary
                    
                    var radiosArray : [Radio] = []
                    
                    for radioItem in ((responseDictionary["results"] as! NSDictionary)["app"] as! NSDictionary)["radios"] as! NSArray {
                        
                        let radio : Radio = Radio().parseRadioFromDict(radioDict: (radioItem as! NSDictionary))
                        radiosArray.append(radio)
                    
                    }
                    
                    callback (nil, radiosArray)
                    
                    break
                    
                case .failure(let error):
                    print("\n ****** getRadios() -> Error: \(error)")
                    callback(error as NSError?,nil)
                    break
                }
        }
        
        
    }
    
    public func getNews(
        page : Int,
        items: Int,
        imageWidth : Int,
        imageHeight: Int,
        callback: @escaping (NSError?,[News]?,PagingInfo?) -> ()){
        
        print("\n *** getNews()")
        
        Alamofire.request(
            kURL_NEWS + "items=\(items)&page=\(page)&width=\(imageWidth)&height=\(imageHeight)")
            .responseJSON { response in
                
                switch response.result {
                    
                case .success(let JSON) :
                    
                    print("\n ****** getNews() -> Success")
                    
                    let responseDictionary = JSON as! NSDictionary
                    
                    var newsArray : [News] = []
                    
                    for newsItem in (responseDictionary["results"] as! NSDictionary)["news"] as! NSArray {
                        
                        let news : News = News().parseNewsFromDict(newsDict: newsItem as! NSDictionary)
                        newsArray.append(news)
                        
                    }
                    
                    let pagingInfo : PagingInfo = PagingInfo().parsePagingFromDict(pagingDict: (responseDictionary["results"] as! NSDictionary)["paging"] as! NSDictionary)
                    
                    callback (nil, newsArray,pagingInfo)
                    
                    break
                    
                case .failure(let error):
                    print("\n ****** getNews() -> Error: \(error)")
                    callback(error as NSError?,nil,nil)
                    break
                }
        }
        
        
    }
    
    public func getNewsDetail(
        newsID : String,
        callback: @escaping (NSError?,News?) -> ()){
        
        print("\n *** getNewsDetail(\(newsID))")
        
        Alamofire.request(
            kURL_NEWS_DETAIL + newsID)
            .responseJSON { response in
                
                switch response.result {
                    
                case .success(let JSON) :
                    
                    print("\n ****** getNewsDetail(\(newsID)) -> Success")
                    
                    let responseDictionary = JSON as! NSDictionary
  
                    let news : News = News().parseNewsFromDict(newsDict: (responseDictionary["results"] as! NSDictionary)["news_detail"] as! NSDictionary)

                    callback (nil, news)
                    
                    break
                    
                case .failure(let error):
                    print("\n ****** getNewsDetail(\(newsID)) -> Error: \(error)")
                    callback(error as NSError?,nil)
                    break
                }
        }
        
        
    }
    
    public func getFrequencies(
        latitude : String,
        longitude : String,
        callback: @escaping (NSError?,[Frequency]?) -> ()){
        
        print("\n *** getFrequencies()")
        
        Alamofire.request(
            kURL_FREQUENCIES + "latitude=\(latitude)&longitude=\(longitude)&max_level=locality")
            .responseJSON { response in
                
                switch response.result {
                    
                case .success(let JSON) :
                    
                    print("\n ****** getFrequencies() -> Success")
                    
                    let responseDictionary = JSON as! NSDictionary
                    
                    var frequenciesArray : [Frequency] = []
                    
                    for frequencyItem in (responseDictionary["results"] as! NSDictionary)["frequencies"] as! NSArray {
                        
                        let frequency : Frequency = Frequency().parseFrequencyFromDict(frequencyDict: frequencyItem as! NSDictionary)
                        frequenciesArray.append(frequency)
                        
                    }
                    
                    callback (nil, frequenciesArray)
                    
                    break
                    
                case .failure(let error):
                    print("\n ****** getFrequencies() -> Error: \(error)")
                    callback(error as NSError?,nil)
                    break
                }
        }
        
        
    }
    
    public func getFrequencies(
        radioID : String,
        locality : String,
        callback: @escaping (NSError?,[Frequency]?) -> ()){
        
        print("\n *** getFrequenciesGeolocated(\(radioID),\(locality))")
        
        Alamofire.request(
            kURL_RADIO_FREQUENCIES + radioID + "/geolocate?city=\(locality)")
            .responseJSON { response in
                
                switch response.result {
                    
                case .success(let JSON) :
                    
                    print("\n ****** getFrequenciesGeolocated(\(radioID),\(locality)) -> Success")
                    
                    let responseDictionary = JSON as! NSDictionary
                    
                    var frequenciesArray : [Frequency] = []
                    
                    for frequencyItem in (responseDictionary["results"] as! NSDictionary)["frequencies"] as! NSArray {
                        
                        let frequency : Frequency = Frequency().parseFrequencyFromDict(frequencyDict: frequencyItem as! NSDictionary)
                        frequenciesArray.append(frequency)
                        
                    }
                    
                    callback (nil, frequenciesArray)
                    
                    break
                    
                case .failure(let error):
                    print("\n ****** getFrequenciesGeolocated(\(radioID),\(locality)) -> Error: \(error)")
                    callback(error as NSError?,nil)
                    break
                }
        }
        
        
    }
}
