//
//  Player.swift
//  FMWorldSDK
//
//  Created by Oronzo Marrazzo on 16/01/17.
//  Copyright © 2017 Oronzo Marrazzo. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import MediaPlayer

public protocol PlayerDelegate {
    func playerStatusChanged(isPlaying: Bool)
    func metadataChanged(metadata: String)
}

public class Player : NSObject, AVAudioPlayerDelegate {
    
    public var delegate:PlayerDelegate?
    public static let sharedInstance = Player()
    
    var audioPlayer : AVPlayer = AVPlayer()
    public var isPlaying : Bool = false
    var metadata : String = ""
    var radio : Radio = Radio()
    
    
    
    public func setPlayer (radio : Radio, delegate: PlayerDelegate?){
        
        self.radio = radio
    
        if delegate != nil {
            self.delegate = delegate
        }
        
        
        self.audioPlayer = AVPlayer(url: URL(string: (radio.streams?[0].url!)!)!)
        
        self.setRemoteContents()
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            UIApplication.shared.beginReceivingRemoteControlEvents()
            self.setRemoteController()
            print("Receiving remote control events\n")
        } catch {
            print("Audio Session error.\n")
        }
    }
    
    public func setDelegate (delegate : PlayerDelegate){
        self.delegate = delegate
    }
    
    public func play(radio : Radio, delegate: PlayerDelegate?) {
        
        self.setPlayer(radio: radio, delegate: delegate)
        
        self.audioPlayer.play()
        self.isPlaying = true
        self.delegate?.playerStatusChanged(isPlaying: self.isPlaying)
        
        self.audioPlayer.currentItem!.addObserver(self, forKeyPath: "timedMetadata", options: .new, context: nil)
        
        if self.metadata != "" {
            self.delegate?.metadataChanged(metadata: self.metadata)
        }
    
    }
    
    public func play() {
        self.audioPlayer.play()
        self.isPlaying = true
        self.delegate?.playerStatusChanged(isPlaying: self.isPlaying)
        
        if self.metadata != "" {
            self.delegate?.metadataChanged(metadata: self.metadata)
        }
        
    }
    
    public func pause() {
        self.audioPlayer.pause()
        self.isPlaying = false
        self.delegate?.playerStatusChanged(isPlaying: self.isPlaying)
        
    }
    
    func setRemoteContents() {
        
        var informationDict : [String : Any] = [:]
        informationDict[MPMediaItemPropertyAlbumTitle] = self.radio.name ?? ""
        informationDict[MPMediaItemPropertyArtist] = self.metadata
        
        if radio.imageUrl != nil {
            do{
                let image = try UIImage(data: Data(contentsOf: URL(string: radio.imageUrl!)!))
                let albumArtwork = MPMediaItemArtwork(boundsSize: (image?.size)!, requestHandler: { (size) -> UIImage in
                    return image!
                })
                informationDict[MPMediaItemPropertyArtwork] = albumArtwork
                
            }catch{}
            
        }
        
        MPNowPlayingInfoCenter.default().nowPlayingInfo = informationDict
        
    }
    
    func setRemoteController () {
        
        let mpController : MPRemoteCommandCenter = MPRemoteCommandCenter.shared()
        
        mpController.playCommand.addTarget(self, action: #selector(remotePlay))
        mpController.pauseCommand.addTarget(self, action: #selector(remotePause))
        mpController.togglePlayPauseCommand.addTarget(self, action: #selector(remoteToggle))
        
    }
    
    
    func remotePlay() {
        print("remotePlay")
        self.play()
    }
    func remotePause() {
        print("remotePause")
        self.pause()
    }
    func remoteToggle() {
        print("remoteToggle")
        if self.isPlaying == true {
            self.pause()
        }else{
            self.play()
        }
        
    }
    
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "timedMetadata" {
        
            let playerItem : AVPlayerItem = object as! AVPlayerItem
            
            if let items = playerItem.timedMetadata {
                
                for item in items {
                    
                    let data : Data = ((item.value as! String).data(using: .isoLatin1)!)
                    let metadataString : String = String(data: data, encoding: .utf8)!
                    self.metadata = metadataString
                    self.delegate?.metadataChanged(metadata: self.metadata)
                    self.setRemoteContents()
                    
                }
                
            }
            
        }
    }
}
